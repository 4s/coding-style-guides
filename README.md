# Coding style guides

A collection of configuration files supporting coding formatting and checking of coding styles in various IDEs and 
other tools.


The style used for Java originates in https://github.com/google/styleguide, but has been customized in the 
following ways:

  - Block indentation is 4 spaces (was 2)
  - Column limit is 120 (was 100)

## Intellij IDEA

  - Open *Preference : Editor : Code Style : Java*
  - Select *Import Scheme* 
  - Select the file intellij-java-style.xml

## Installation in Eclipse
TBD

## Maven checkstyle plugin
The file checkstyle.xml can be used with the [Apache Maven Checkstyle Plugin](https://maven.apache.org/plugins/maven-checkstyle-plugin/index.html)

Example POM configuration:

```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-checkstyle-plugin</artifactId>
    <version>2.12.1</version>
    <executions>
        <execution>
            <configuration>
                <configLocation>https://bitbucket.org/4s/coding-style-guides/raw/master/checkstyle.xml</configLocation>
                <consoleOutput>true</consoleOutput>
                <failsOnError>false</failsOnError>
            </configuration>
            <goals>
                <goal>check</goal>
            </goals>
        </execution>
    </executions>
</plugin>
```

For how to run the checks see [Plugin Documentation](https://maven.apache.org/plugins/maven-checkstyle-plugin/plugin-info.html)

Results of check are available at `target/checkstyle-result.xml` and `target/site/checkstyle.html`.